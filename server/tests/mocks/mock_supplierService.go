// Code generated by MockGen. DO NOT EDIT.
// Source: ./services/supplierService.go

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	types "github.com/sean-david-welch/farmec-v2/server/types"
)

// MockSupplierService is a mock of SupplierService interface.
type MockSupplierService struct {
	ctrl     *gomock.Controller
	recorder *MockSupplierServiceMockRecorder
}

// MockSupplierServiceMockRecorder is the mock recorder for MockSupplierService.
type MockSupplierServiceMockRecorder struct {
	mock *MockSupplierService
}

// NewMockSupplierService creates a new mock instance.
func NewMockSupplierService(ctrl *gomock.Controller) *MockSupplierService {
	mock := &MockSupplierService{ctrl: ctrl}
	mock.recorder = &MockSupplierServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockSupplierService) EXPECT() *MockSupplierServiceMockRecorder {
	return m.recorder
}

// CreateSupplier mocks base method.
func (m *MockSupplierService) CreateSupplier(supplier *types.Supplier) (*types.SupplierResult, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateSupplier", supplier)
	ret0, _ := ret[0].(*types.SupplierResult)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateSupplier indicates an expected call of CreateSupplier.
func (mr *MockSupplierServiceMockRecorder) CreateSupplier(supplier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateSupplier", reflect.TypeOf((*MockSupplierService)(nil).CreateSupplier), supplier)
}

// DeleteSupplier mocks base method.
func (m *MockSupplierService) DeleteSupplier(id string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteSupplier", id)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteSupplier indicates an expected call of DeleteSupplier.
func (mr *MockSupplierServiceMockRecorder) DeleteSupplier(id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteSupplier", reflect.TypeOf((*MockSupplierService)(nil).DeleteSupplier), id)
}

// GetSupplierById mocks base method.
func (m *MockSupplierService) GetSupplierById(id string) (*types.Supplier, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSupplierById", id)
	ret0, _ := ret[0].(*types.Supplier)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSupplierById indicates an expected call of GetSupplierById.
func (mr *MockSupplierServiceMockRecorder) GetSupplierById(id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSupplierById", reflect.TypeOf((*MockSupplierService)(nil).GetSupplierById), id)
}

// GetSuppliers mocks base method.
func (m *MockSupplierService) GetSuppliers() ([]types.Supplier, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSuppliers")
	ret0, _ := ret[0].([]types.Supplier)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSuppliers indicates an expected call of GetSuppliers.
func (mr *MockSupplierServiceMockRecorder) GetSuppliers() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSuppliers", reflect.TypeOf((*MockSupplierService)(nil).GetSuppliers))
}

// UpdateSupplier mocks base method.
func (m *MockSupplierService) UpdateSupplier(id string, supplier *types.Supplier) (*types.SupplierResult, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateSupplier", id, supplier)
	ret0, _ := ret[0].(*types.SupplierResult)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// UpdateSupplier indicates an expected call of UpdateSupplier.
func (mr *MockSupplierServiceMockRecorder) UpdateSupplier(id, supplier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateSupplier", reflect.TypeOf((*MockSupplierService)(nil).UpdateSupplier), id, supplier)
}
